﻿namespace pagination
{
    partial class Customize
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lvitems = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btn1 = new System.Windows.Forms.Button();
            this.btn2 = new System.Windows.Forms.Button();
            this.btnnext = new System.Windows.Forms.Button();
            this.btnlast = new System.Windows.Forms.Button();
            this.btnprev = new System.Windows.Forms.Button();
            this.lblpages = new System.Windows.Forms.Label();
            this.btnfirst = new System.Windows.Forms.Button();
            this.btn3 = new System.Windows.Forms.Button();
            this.btngoto = new System.Windows.Forms.Button();
            this.numIndex = new System.Windows.Forms.NumericUpDown();
            this.lblitems = new System.Windows.Forms.Label();
            this.lblitemPerPage = new System.Windows.Forms.Label();
            this.dgItems = new System.Windows.Forms.DataGridView();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Qty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numIndex)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgItems)).BeginInit();
            this.SuspendLayout();
            // 
            // lvitems
            // 
            this.lvitems.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3});
            this.lvitems.FullRowSelect = true;
            this.lvitems.GridLines = true;
            this.lvitems.Location = new System.Drawing.Point(12, 33);
            this.lvitems.Name = "lvitems";
            this.lvitems.Size = new System.Drawing.Size(308, 392);
            this.lvitems.TabIndex = 3;
            this.lvitems.UseCompatibleStateImageBehavior = false;
            this.lvitems.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "ID";
            this.columnHeader1.Width = 68;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "itemname";
            this.columnHeader2.Width = 127;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "qty";
            this.columnHeader3.Width = 107;
            // 
            // btn1
            // 
            this.btn1.Location = new System.Drawing.Point(273, 443);
            this.btn1.Name = "btn1";
            this.btn1.Size = new System.Drawing.Size(35, 31);
            this.btn1.TabIndex = 4;
            this.btn1.Text = "1";
            this.btn1.UseVisualStyleBackColor = true;
            this.btn1.Click += new System.EventHandler(this.btn1_Click);
            // 
            // btn2
            // 
            this.btn2.Location = new System.Drawing.Point(314, 443);
            this.btn2.Name = "btn2";
            this.btn2.Size = new System.Drawing.Size(35, 31);
            this.btn2.TabIndex = 5;
            this.btn2.Text = "2";
            this.btn2.UseVisualStyleBackColor = true;
            this.btn2.Click += new System.EventHandler(this.btn2_Click);
            // 
            // btnnext
            // 
            this.btnnext.Location = new System.Drawing.Point(396, 443);
            this.btnnext.Name = "btnnext";
            this.btnnext.Size = new System.Drawing.Size(35, 31);
            this.btnnext.TabIndex = 6;
            this.btnnext.Text = ">>";
            this.btnnext.UseVisualStyleBackColor = true;
            this.btnnext.Click += new System.EventHandler(this.btnnext_Click);
            // 
            // btnlast
            // 
            this.btnlast.Location = new System.Drawing.Point(437, 443);
            this.btnlast.Name = "btnlast";
            this.btnlast.Size = new System.Drawing.Size(35, 31);
            this.btnlast.TabIndex = 7;
            this.btnlast.Text = "last";
            this.btnlast.UseVisualStyleBackColor = true;
            this.btnlast.Click += new System.EventHandler(this.btnlast_Click);
            // 
            // btnprev
            // 
            this.btnprev.Location = new System.Drawing.Point(230, 443);
            this.btnprev.Name = "btnprev";
            this.btnprev.Size = new System.Drawing.Size(35, 31);
            this.btnprev.TabIndex = 8;
            this.btnprev.Text = "<<";
            this.btnprev.UseVisualStyleBackColor = true;
            this.btnprev.Click += new System.EventHandler(this.btnprev_Click);
            // 
            // lblpages
            // 
            this.lblpages.AutoSize = true;
            this.lblpages.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblpages.Location = new System.Drawing.Point(374, 494);
            this.lblpages.Name = "lblpages";
            this.lblpages.Size = new System.Drawing.Size(36, 20);
            this.lblpages.TabIndex = 9;
            this.lblpages.Text = "of n";
            // 
            // btnfirst
            // 
            this.btnfirst.Location = new System.Drawing.Point(190, 443);
            this.btnfirst.Name = "btnfirst";
            this.btnfirst.Size = new System.Drawing.Size(35, 31);
            this.btnfirst.TabIndex = 10;
            this.btnfirst.Text = "first";
            this.btnfirst.UseVisualStyleBackColor = true;
            this.btnfirst.Click += new System.EventHandler(this.btnfirst_Click);
            // 
            // btn3
            // 
            this.btn3.Location = new System.Drawing.Point(355, 443);
            this.btn3.Name = "btn3";
            this.btn3.Size = new System.Drawing.Size(35, 31);
            this.btn3.TabIndex = 11;
            this.btn3.Text = "3";
            this.btn3.UseVisualStyleBackColor = true;
            this.btn3.Click += new System.EventHandler(this.btn3_Click);
            // 
            // btngoto
            // 
            this.btngoto.Location = new System.Drawing.Point(251, 488);
            this.btngoto.Name = "btngoto";
            this.btngoto.Size = new System.Drawing.Size(76, 31);
            this.btngoto.TabIndex = 13;
            this.btngoto.Text = "go to page";
            this.btngoto.UseVisualStyleBackColor = true;
            this.btngoto.Click += new System.EventHandler(this.btngoto_Click);
            // 
            // numIndex
            // 
            this.numIndex.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numIndex.Location = new System.Drawing.Point(333, 490);
            this.numIndex.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numIndex.Name = "numIndex";
            this.numIndex.Size = new System.Drawing.Size(41, 27);
            this.numIndex.TabIndex = 14;
            this.numIndex.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numIndex.UpDownAlign = System.Windows.Forms.LeftRightAlignment.Left;
            this.numIndex.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // lblitems
            // 
            this.lblitems.AutoSize = true;
            this.lblitems.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblitems.Location = new System.Drawing.Point(12, 447);
            this.lblitems.Name = "lblitems";
            this.lblitems.Size = new System.Drawing.Size(18, 20);
            this.lblitems.TabIndex = 15;
            this.lblitems.Text = "n";
            // 
            // lblitemPerPage
            // 
            this.lblitemPerPage.AutoSize = true;
            this.lblitemPerPage.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblitemPerPage.Location = new System.Drawing.Point(12, 486);
            this.lblitemPerPage.Name = "lblitemPerPage";
            this.lblitemPerPage.Size = new System.Drawing.Size(18, 20);
            this.lblitemPerPage.TabIndex = 16;
            this.lblitemPerPage.Text = "n";
            // 
            // dgItems
            // 
            this.dgItems.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgItems.BackgroundColor = System.Drawing.Color.White;
            this.dgItems.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgItems.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID,
            this.ItemName,
            this.Qty});
            this.dgItems.Location = new System.Drawing.Point(338, 33);
            this.dgItems.Name = "dgItems";
            this.dgItems.RowHeadersVisible = false;
            this.dgItems.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dgItems.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgItems.Size = new System.Drawing.Size(308, 392);
            this.dgItems.TabIndex = 17;
            // 
            // ID
            // 
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            // 
            // ItemName
            // 
            this.ItemName.HeaderText = "ItemName";
            this.ItemName.Name = "ItemName";
            // 
            // Qty
            // 
            this.Qty.HeaderText = "Qty";
            this.Qty.Name = "Qty";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(8, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 20);
            this.label1.TabIndex = 18;
            this.label1.Text = "ListView";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(334, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(108, 20);
            this.label2.TabIndex = 19;
            this.label2.Text = "DataGridView";
            // 
            // PaginationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(660, 529);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dgItems);
            this.Controls.Add(this.lblitemPerPage);
            this.Controls.Add(this.lblitems);
            this.Controls.Add(this.numIndex);
            this.Controls.Add(this.btngoto);
            this.Controls.Add(this.btn3);
            this.Controls.Add(this.btnfirst);
            this.Controls.Add(this.btnprev);
            this.Controls.Add(this.btnlast);
            this.Controls.Add(this.btnnext);
            this.Controls.Add(this.btn2);
            this.Controls.Add(this.btn1);
            this.Controls.Add(this.lvitems);
            this.Controls.Add(this.lblpages);
            this.MaximizeBox = false;
            this.Name = "PaginationForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = " Pagination";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numIndex)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgItems)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView lvitems;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.Button btn1;
        private System.Windows.Forms.Button btn2;
        private System.Windows.Forms.Button btnnext;
        private System.Windows.Forms.Button btnlast;
        private System.Windows.Forms.Button btnprev;
        private System.Windows.Forms.Label lblpages;
        private System.Windows.Forms.Button btnfirst;
        private System.Windows.Forms.Button btn3;
        private System.Windows.Forms.Button btngoto;
        private System.Windows.Forms.NumericUpDown numIndex;
        private System.Windows.Forms.Label lblitems;
        private System.Windows.Forms.Label lblitemPerPage;
        private System.Windows.Forms.DataGridView dgItems;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Qty;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}

