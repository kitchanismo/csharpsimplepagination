﻿namespace pagination
{
    partial class Basic
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lvitems = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnprev = new System.Windows.Forms.Button();
            this.btnnext = new System.Windows.Forms.Button();
            this.lblpages = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lvitems
            // 
            this.lvitems.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3});
            this.lvitems.FullRowSelect = true;
            this.lvitems.GridLines = true;
            this.lvitems.Location = new System.Drawing.Point(25, 24);
            this.lvitems.Name = "lvitems";
            this.lvitems.Size = new System.Drawing.Size(308, 392);
            this.lvitems.TabIndex = 4;
            this.lvitems.UseCompatibleStateImageBehavior = false;
            this.lvitems.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "ID";
            this.columnHeader1.Width = 68;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "itemname";
            this.columnHeader2.Width = 127;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "qty";
            this.columnHeader3.Width = 107;
            // 
            // btnprev
            // 
            this.btnprev.Location = new System.Drawing.Point(257, 422);
            this.btnprev.Name = "btnprev";
            this.btnprev.Size = new System.Drawing.Size(35, 31);
            this.btnprev.TabIndex = 11;
            this.btnprev.Text = "<<";
            this.btnprev.UseVisualStyleBackColor = true;
            this.btnprev.Click += new System.EventHandler(this.btnprev_Click);
            // 
            // btnnext
            // 
            this.btnnext.Location = new System.Drawing.Point(298, 422);
            this.btnnext.Name = "btnnext";
            this.btnnext.Size = new System.Drawing.Size(35, 31);
            this.btnnext.TabIndex = 10;
            this.btnnext.Text = ">>";
            this.btnnext.UseVisualStyleBackColor = true;
            this.btnnext.Click += new System.EventHandler(this.btnnext_Click);
            // 
            // lblpages
            // 
            this.lblpages.AutoSize = true;
            this.lblpages.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblpages.Location = new System.Drawing.Point(284, 473);
            this.lblpages.Name = "lblpages";
            this.lblpages.Size = new System.Drawing.Size(49, 20);
            this.lblpages.TabIndex = 12;
            this.lblpages.Text = "n of n";
            // 
            // Basic
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(359, 501);
            this.Controls.Add(this.btnprev);
            this.Controls.Add(this.btnnext);
            this.Controls.Add(this.lblpages);
            this.Controls.Add(this.lvitems);
            this.Name = "Basic";
            this.Text = "Basic";
            this.Load += new System.EventHandler(this.Basic_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView lvitems;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.Button btnprev;
        private System.Windows.Forms.Button btnnext;
        private System.Windows.Forms.Label lblpages;
    }
}