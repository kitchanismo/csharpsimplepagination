﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace pagination
{
    public partial class Basic : Form
    {
        KitchanismoPaginate<Product> paginate;

        int _page = 1;
        int _countPage;

        public Basic()
        {
            InitializeComponent();
            paginate = new KitchanismoPaginate<Product>(Database.Connection);
            InitializePagination();
        }

        void InitializePagination()
        {
            //field to sortby
            paginate.OrderBy = "id"; //new Product().id.GetType().Name;

            //initialize number of item per page
            paginate.Take = 15; 

            //assign to local variable
            _countPage = paginate.CountPage();
        }

        private void Basic_Load(object sender, EventArgs e)
        {
            //first page
            NavigatePage(_page);
        }

        private void btnnext_Click(object sender, EventArgs e)
        {
            //increment | next page
            NavigatePage(++_page);
        }

        private void btnprev_Click(object sender, EventArgs e)
        {
            //decrement | previous page
            NavigatePage(--_page);
        }

        //do some validation
        private void NavigatePage(int page)
        {
            if (page > _countPage)
            {
                _page = _countPage;
                return;
            }
            if (page == 0)
            {
                _page = 1;
                return;
            }

            paginate.Load(lvitems, _page);
            lblpages.Text = _page + " of " + _countPage;
        }

    }
}
