﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace pagination
{
    public partial class Customize : Form
    {
        private KitchanismoPaginate<Product> paginate;

        private const int FIRST_PAGE = 1;
        private int _countPage;

        public Customize()
        {
            InitializeComponent();
            paginate = new KitchanismoPaginate<Product>(Database.Connection);
            InitializePagination();
            VisiblePagesButton();
        }

        void InitializePagination()
        {
            paginate.OrderBy = "id";
            paginate.Take    = 20; 
            _countPage       = paginate.CountPage();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            numIndex.Maximum    = _countPage;
            lblitems.Text       = "Items: " + paginate.CountRows().ToString();
            lblitemPerPage.Text = "Take: "  + paginate.Take.ToString();
            CurrentPage(FIRST_PAGE);
        }

        private void NavigatePage(int i)
        {
            CurrentPage(Int32.Parse(btn3.Text) + i);

            btn1.Text = (Int32.Parse(btn1.Text) + i).ToString();
            btn2.Text = (Int32.Parse(btn2.Text) + i).ToString();
            btn3.Text = (Int32.Parse(btn3.Text) + i).ToString();
        }

        private void CurrentPage(int _pageNum)
        {
            numIndex.Value = _pageNum;
            lblpages.Text = "of " + _countPage;
            paginate.Load(lvitems, _pageNum);
            paginate.Load(dgItems, _pageNum);
        }

      
        private void btnlast_Click(object sender, EventArgs e)
        {
            CurrentPage(_countPage);

            btn1.Text = (_countPage - 2).ToString();
            btn2.Text = (_countPage - 1).ToString();
            btn3.Text = _countPage.ToString();
        }

        private void btnfirst_Click(object sender, EventArgs e)
        {
            CurrentPage(FIRST_PAGE);

            btn1.Text = "1";
            btn2.Text = "2";
            btn3.Text = "3";
        }

        private void btn1_Click(object sender, EventArgs e)
        {
            CurrentPage(Int32.Parse(btn1.Text));
        }

        private void btn2_Click(object sender, EventArgs e)
        {
            CurrentPage(Int32.Parse(btn2.Text));
        }
        private void btn3_Click(object sender, EventArgs e)
        {
            CurrentPage(Int32.Parse(btn3.Text));
        }
       
        private void btnnext_Click(object sender, EventArgs e)
        {
            var pages = _countPage.ToString();

            if (pages != btn3.Text)
            {
                NavigatePage(1);
            }
        }

        private void btnprev_Click(object sender, EventArgs e)
        {
            if (btn1.Text == "1")
            {
                CurrentPage(FIRST_PAGE);
            }
            else
            {
                NavigatePage(-1);
            }
        }

        private void btngoto_Click(object sender, EventArgs e)
        {
            var _pageNum = Int32.Parse(numIndex.Value.ToString());
            var pages = _countPage;

            if (_pageNum == Int32.Parse(btn3.Text))
            {
                return;
            }

            if (_pageNum < 4)
            {
                CurrentPage(_pageNum);
                btn1.Text = "1";
                btn2.Text = "2";
                btn3.Text = "3";
                return;
            }

            if (_pageNum <= pages)
            {
                CurrentPage(_pageNum);
                btn1.Text = (_pageNum - 2).ToString();
                btn2.Text = (_pageNum - 1).ToString();
                btn3.Text = _pageNum.ToString();
                return;
            }
            
        }

        private void VisiblePagesButton()
        {
            if (_countPage == 1)
            {
                btn2.Visible = false;
            }
            if (_countPage == 2)
            {
                btn3.Visible = false;
            }
        }

    }
}
